<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->longText('api_token')->nullable()->after('remember_token');
            $table->boolean('is_admin')->default(0);
            $table->boolean('is_staff')->default(0);
            $table->boolean('is_customer')->default(1);
            $table->boolean('is_active')->default(1);
            $table->string('profile_pic')->default('profile.png')->nullable()->after('is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
