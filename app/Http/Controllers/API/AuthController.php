<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewUserNotification;
use App\Mail\PasswordResetNotification;
use App\Models\PasswordReset;
use Carbon\Carbon;
use Dirape\Token\Token;
use Log;

class AuthController extends Controller
{
    /**
     * Validations for create user
     */
    private $create_login_rules = [ 
        'email'     => 'required|email',
        'password'  => 'required',
    ];

        /**
     * Validations for create user
     */
    private $create_user_rules = [ 
        'name'      => 'required',
        'email'     => 'required|email|unique:users,email',
        'password'  => 'required|min:6',
        
    ];

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), $this->create_login_rules);
     
        if ($validator->fails()) { 
            $message = $validator->errors();
            return response()->json([
            'message' => 'The email must be a valid email address.',
        ], 400);
        }

        $user = User::where('email','=',$request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                if ($user->is_admin == 1) {

                    auth()->logout();
                    session()->flush();

                    return $this->responseError("Unauthorize access", 400);
                }
                if ($user->is_active == 0) {

                    auth()->logout();
                    session()->flush();

                    return $this->responseError(trans('messages.Sorry, your account is not activated by admin'), 400);
                }
                if ($user->is_active == 2) {

                    auth()->logout();
                    session()->flush();

                    return $this->responseError(trans('messages.Sorry, your account has been inactive'), 400);

                } 
                if ($user->is_active == 3) {

                    auth()->logout();
                    session()->flush();

                    return $this->responseError(trans('messages.Sorry, your account has been Suspended by admin'), 400);

                }
                $accessToken = $user->createToken('authToken')->accessToken;

                $users = $user->update([
                    'updated_at' => time(),
                    'api_token' => $accessToken
                ]);

                return response(['messages' => 'User logged In Successfully', 'user' => $user, 'access_token' => $accessToken]);
            } else {
                return response(['message' => 'Your Password is Incorrect.'], 400);
            }
        } else {
            return response(['message' => 'The provided Email do not match our records.'], 400);
        }
        
    }

    /**
     * Register User
     */
    public function register(Request $request) {

        $validator = Validator::make($request->all(), $this->create_user_rules);
     
        if ($validator->fails()) { 
            $message = $validator->errors();
            return response()->json([
                'status'  => 400,
                'message' => $message,
            ], 400);
        }
        
        $user = $this->createUser($request);
        $accessToken = $user->createToken('authToken')->accessToken;
        $responseData = [
            'token' => $accessToken,
        ];
        return response()->json([
            'status'  => 200,
            'message' => $responseData,
            'data'    => 1
        ], 200);
    }
    /**
     * Get user inputs
     */
    public function createUser($request) {
    
        $user = new User();
        $user->is_customer  = 1;
        $user->is_active    = 1;
        $user->email        = $request->email;
        $user->password     = Hash::make($request->password);
        $user->name         = $request->name;

        $user->save();

         $data1['email']          = $request->email;
         $data1['email_title']    = 'Welcome to our project . Hi '.$request->name.' You are now register in our project';
         //mail goes to user
         Mail::to($data1['email'])->send(new NewUserNotification($data1));
        return $user;
    }
    /**
     * Log out
     */
    public function logout(Request $request)
    {
        $user = auth()->user();
        $token = $user->token();
        $token->revoke();

        return $this->responseSuccess('You have logged out successfully');
    }
    /**
     * forgot_password
     */
    public function forgot_password(Request $request)
    {
        $input = $request->all();
        $email = $request->email;
        $rules = array(
            'email' => "required|email",
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            if(user::where('email',  $email)->exists())
            {
               $otp =  rand ( 1000 , 9999);
               $new = new PasswordReset();
               $new->email  = $email;
               $new->otp    = $otp;
               $new->save();
               
                $data['email']          = $email;
                $data['otp']            = $otp;
                $data['email_title']    = 'You are receiving this email because we received a password reset request for your account..';
               
                //mail goes to user
                Mail::to($data['email'])->send(new PasswordResetNotification($data));
                // check for failed ones
                if (Mail::failures()) {
                    // return failed mails
                    $arr = array("status" => 400, "message" => Mail::failures(), "data" => array());
                }
                $arr = array("status" => 200, "message" => 'Password Reset OTP Sent Successfully');
            }else{
                $arr = array("status" => 400, "message" => 'This Email is not exists');
            }
        }
        return \Response::json($arr);
    }
    /**
     * verifyOTP
     */
    public function verifyOTP(Request $request)
    {
        $input  = $request->all();
        $email  = $request->email;
        $otp    = $request->otp;
        $rules  = array(
            'email' => "required|email",
            'otp' => "required"
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
        $verify = PasswordReset::where('email', $email)->where('otp', $otp)->first();
        if(isset($verify))
        {
            $time = $verify->created_at;
            $carbon = Carbon::now();
            $diff_in_minutes = $carbon->diffInMinutes($time);
            Log::info($diff_in_minutes);
            if ($diff_in_minutes < 1){
                $token = (new Token())->Unique('users', 'api_token', 60);
                $user = User::where('email', $email)->first();
                $user->api_token = $token;
                $user->save();
                $arr = array("status" => 200, "message" => 'Otp Matched', 'reset_token' =>$token);
        }else
            {
                $arr = array("status" => 400, "message" => 'Your OTP is Expired');
            }
        }else{
            $arr = array("status" => 400, "message" => 'Entered OTP is not matched');
        }
    }
        return \Response::json($arr);
    }
    /**
     * changePasswordWithToken
     */
    public function changePasswordWithToken(Request $request)
    {
        $input = $request->all();
        $rules = array(
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
        } else {
            $details = User::where('api_token', $request->reset_token)->first();
            $details->password =  Hash::make($request->new_password);
            $details->save();
            $arr = array("status" => 200, "message" => 'Password Change Successfully');
        }
        return \Response::json($arr);
    }
}
