<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>


<div>
    Hi  {{$email_title}}.
    <br>
    <br>
    {{$otp}}.
    <br>
    <br>
    This password reset OTP will expire in 60 minutes..
    <br>
    If you did not request a password reset, no further action is required.
    <br>
    <br>
    Kind Regards,<br>
    Team {{ env('APP_NAME') }}

</div>

</body>
</html>